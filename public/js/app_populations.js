/**
 * Реализация API, не изменяйте ее
 * @param {string} url
 * @param {function} callback
 */
function getData(url, callback) {
    var RESPONSES = {
        '/countries': [
            {name: 'Cameroon', continent: 'Africa'},
            {name :'Fiji Islands', continent: 'Oceania'},
            {name: 'Guatemala', continent: 'North America'},
            {name: 'Japan', continent: 'Asia'},
            {name: 'Yugoslavia', continent: 'Europe'},
            {name: 'Tanzania', continent: 'Africa'}
        ],
        '/cities': [
            {name: 'Bamenda', country: 'Cameroon'},
            {name: 'Suva', country: 'Fiji Islands'},
            {name: 'Quetzaltenango', country: 'Guatemala'},
            {name: 'Osaka', country: 'Japan'},
            {name: 'Subotica', country: 'Yugoslavia'},
            {name: 'Zanzibar', country: 'Tanzania'},
        ],
        '/populations': [
            {count: 138000, name: 'Bamenda'},
            {count: 77366, name: 'Suva'},
            {count: 90801, name: 'Quetzaltenango'},
            {count: 2595674, name: 'Osaka'},
            {count: 100386, name: 'Subotica'},
            {count: 157634, name: 'Zanzibar'}
        ]
    };

    setTimeout(function () {
        var result = RESPONSES[url];
        if (!result) {
            return callback('Unknown url');
        }

        callback(null, result);
    }, Math.round(Math.random * 1000));
}

/**
 * Ваши изменения ниже
 */
var requests = ['/countries', '/cities', '/populations'];
var responses = {};

var geoObject = prompt('Введите название страны или города:','Cameroon');

for (i = 0; i < 3; i++) {
    var request = requests[i];
    var callback = function (error, result) {
		if(!error) {
			//responses[request] = result; ошибка тут, так как функция callback выполняется с задержкой, цикл успевает выполнится все 3 раза
			// в момент выполняния кода внутри setTimeout request всегда будет иметь значение /populations    
			
			responses[requests[0]] = result; //простой фикс это получение первого запроса из спикса как текущего
			requests.shift();//а затем его удаление со смещением
			//однако если запросы выполнятся не попоряку, то скрипт сработет не корректно, но при текущей постановке задачи не удалось решить эту проблему
		
			var l = [];
			for (K in responses)
				l.push(K);

			if (l.length == 3) {
				var c = [], cc = [], p = 0;
				
				if(geoObject && geoObject.length > 0) { //код для запросов через окно
					
					for (i = 0; i < responses['/countries'].length; i++) {
						if (responses['/countries'][i].continent === geoObject) {
							c.push(responses['/countries'][i].name);
						}
					}
					
					for (i = 0; i < responses['/cities'].length; i++) {
						if(c.length > 0) {
							for (j = 0; j < c.length; j++) {
								if (responses['/cities'][i].country === c[j]) {
									cc.push(responses['/cities'][i].name);
								}
							}
						}else {
							if (responses['/cities'][i].country === geoObject) {
								cc.push(responses['/cities'][i].name);
							}
						}
					}

					for (i = 0; i < responses['/populations'].length; i++) {
						if(cc.length > 0) {
							for (j = 0; j < cc.length; j++) {
								if (responses['/populations'][i].name === cc[j]) {
									p += responses['/populations'][i].count;
								}
							}
						}else {
							if (responses['/populations'][i].name === geoObject) {
								p += responses['/populations'][i].count;
							}
						}
					}
				
					if(p > 0) {
						console.log('Total population in ' + geoObject + ': ' + p);
					}else {
						console.log('No data about population in the ' + geoObject);
					}
					
				}else { //старый код
					
					for (i = 0; i < responses['/countries'].length; i++) {
							if (responses['/countries'][i].continent === 'Africa') {
								c.push(responses['/countries'][i].name);
							}
					}

					for (i = 0; i < responses['/cities'].length; i++) {
						for (j = 0; j < c.length; j++) {
							if (responses['/cities'][i].country === c[j]) {
								cc.push(responses['/cities'][i].name);
							}
						}
					}

					for (i = 0; i < responses['/populations'].length; i++) {
						for (j = 0; j < cc.length; j++) {
							if (responses['/populations'][i].name === cc[j]) {
								p += responses['/populations'][i].count;
							}
						}
					}
				
					console.log('Total population in Africa: ' + p);
				}
			}
		}else {
			console.log('API вернул ошибку: ' + error);
		}
    };

    getData(request, callback);
}