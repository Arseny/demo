<!DOCTYPE html>
<html lang="ru" id="nojs" xml:lang="ru">
    <head>
        <meta charset="utf-8" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="viewport" content="width=1024" />

        <title>@yield('title')</title>

        <link rel="shortcut icon" href="{!! asset('favicon.ico') !!}" type="image/x-icon" />
		@section('stylesheets')
            <link rel="stylesheet" media="screen" href="{!! asset('css/app_player.css') !!}" />
        @show
    </head>
	<body>
		<div class="page-container">
			<div class="header-stripe stripe-type-overhang"></div>
			<div class="content-stripe stripe-type-overhang">
				<div id="player_container" class="player-container">
						<div class="visualize">
							<canvas id="canvas" width="800" height="350"></canvas>
						</div>
						<div class="botton play" id="play">
						</div>
						<label for="files">
							<div class="botton eject">
							</div>
						</label>
						<input  class="eject-input" type="file" id="files" name="files[]" accept="audio/*" />
						<div class="radio-botton">
							<input type="radio" id="normal-equalizer" name="equalizer" value="normal" checked="checked" />
							<label for="normal-equalizer" id="normal-equalizer-label">
								<span class="label-img"></span><span class="label-text">NORMAL</span>
							</label>
						</div>
						<div class="radio-botton">
							<input type="radio" id="pop-equalizer" name="equalizer" value="pop" />
							<label for="pop-equalizer" id="pop-equalizer-label" >
								<span class="label-img"></span><span class="label-text">POP</span>
							</label>
						</div>
						<div class="radio-botton">
							<input type="radio" id="rock-equalizer" name="equalizer" value="rock" />
							<label for="rock-equalizer" id="rock-equalizer-label" >
								<span class="label-img"></span><span class="label-text">ROCK</span>
							</label>
						</div>
						<div class="radio-botton">
							<input type="radio" id="classic-equalizer" name="equalizer" value="classic" />
							<label for="classic-equalizer" id="classic-equalizer-label">
								<span class="label-img"></span><span class="label-text">CLASSIC</span>
							</label>
						</div>
						<div class="radio-botton">
							<input type="radio" id="jazz-equalizer" name="equalizer" value="jazz" />
							<label for="jazz-equalizer" id="jazz-equalizer-label">
								<span class="label-img"></span><span class="label-text">JAZZ</span>
							</label>
						</div>
						<div class="list" id="list">
						</div>
				</div>
			</div>
		</div>
		<div class="footer-container">
		</div>
		@section('javascripts')
			<script type="text/javascript" src="{!! asset('js/id3.min.js') !!}"></script>
			<script type="text/javascript" src="{!! asset('js/app_player.js') !!}"></script>
		@show
    </body>
</html>
