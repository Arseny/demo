<!DOCTYPE html>
<html lang="ru" id="nojs" xml:lang="ru">
    <head>
        <meta charset="utf-8" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="viewport" content="width=1024" />

        <title>@yield('title')</title>

        <link rel="shortcut icon" href="{!! asset('favicon.ico') !!}" type="image/x-icon" />
		@section('stylesheets')
            <!--[if IE 9 ]>
            <link rel="stylesheet" media="screen" href="{{ asset('css/sl-common.template_styles.ie9.css') }}" />
            <![endif]-->
            <link rel="stylesheet" media="screen" href="{!! asset('css/app.css') !!}" />
        @show
    </head>
	<body>
		<div class="page-container">
			<input class="arrivals-checkbox flight-checkbox" type="checkbox" id="arrivals-checkbox" name="arrivals-checkbox" />
			<label for="arrivals-checkbox"><span></span>Arrivals</label>
			<input class="departures-checkbox flight-checkbox" type="checkbox" id="departures-checkbox" name="departures-checkbox" />
			<label for="departures-checkbox"><span></span>Departures</label>
			<div class="header-stripe stripe-type-overhang">
				<table class="flight-table flight-table-thead">
				    <thead>
						<tr>
							<th class="flight-small-cell">TYPE</th>
							<th class="flight-small-cell">SCHED</th>
							<th class="flight-big-cell">FLIGHT</th>
							<th class="medium-hide flight-big-cell">AIRLINE</th>
							<th class="medium-hide flight-medium-cell">DIRECTION</th>
							<th class="flight-medium-cell">TERMINAL</th>
						</tr>
					</thead>
				</table>
			</div>
			<div class="content-stripe stripe-type-overhang">
				<table class="flight-table">
					<tbody>
						<tr class="flight-row arrivals">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/arrivals-flight.png" title="COMING" /> <p> 09:45 016, Plane: Airbus Industrie A320, Terminal: A, Gate: C64, Direction: Samara, Status:  landed</p>
								</div>
								<img class="flight-logo" src="../image/arrivals-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">09:45</td>
							<td class="flight-big-cell">016 <span class="small-hide">Airbus A320</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">Aeroflot</span><img class="airline-logo" src="../image/aeroflot_full.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Samara</td>
							<td class="flight-medium-cell">A Gate C64</td>
						</tr>
						<tr class="flight-row global-zebra-row departures">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/departures-flight.png" title="COMING" /> <p> 09:55 156, Plane: Boeing 747, Terminal: B, Gate: A50, Direction: Rome, Status: all aboard</p>
								</div>
								<img class="flight-logo" src="../image/departures-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">09:55</td>
							<td class="flight-big-cell">156 <span class="small-hide">Boeing 747</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">Alitalia</span><img class="airline-logo" src="../image/alitalia_big-1.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Rome</td>
							<td class="flight-medium-cell">B Gate A50</td>
						</tr>
						<tr class="flight-row arrivals-zebra-row arrivals">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/arrivals-flight.png" title="COMING" /> <p> 10:00 734, Plane: Boeing 777, Terminal: C, Gate: B50, Direction: Helsinki, Status: expected</p>
								</div>
								<img class="flight-logo" src="../image/arrivals-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">10:00</td>
							<td class="flight-big-cell">734 <span class="small-hide">Boeing 777</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">KLM</span><img class="airline-logo" src="../image/KLM_full.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Helsinki</td>
							<td class="flight-medium-cell">C Gate B50</td>
						</tr>
						<tr class="flight-row global-zebra-row departures departures-zebra-row">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/departures-flight.png" title="COMING" /> <p> 10:10 324, Plane: Airbus Industrie A380, Terminal: A, Gate: A33, Direction: Berlin, Status: registration</p>
								</div>
								<img class="flight-logo" src="../image/departures-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">10:10</td>
							<td class="flight-big-cell">324 <span class="small-hide">Airbus A380</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">KLM</span><img class="airline-logo" src="../image/KLM_full.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Berlin</td>
							<td class="flight-medium-cell">A Gate A33</td>
						</tr>
						<tr class="flight-row departures">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/departures-flight.png" title="COMING" /> <p> 10:25 416, Plane: Airbus Industrie A320, Terminal: D, Gate: B12, Direction: Orenburg, Status: expected</p>
								</div>
								<img class="flight-logo" src="../image/departures-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">10:25</td>
							<td class="flight-big-cell">416 <span class="small-hide">Airbus A320</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">Aeroflot</span><img class="airline-logo" src="../image/aeroflot_full.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Orenburg</td>
							<td class="flight-medium-cell">D Gate B12</td>
						</tr>
						<tr class="flight-row global-zebra-row departures">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/departures-flight.png" title="COMING" /> <p> 10:30 332, Plane: Boeing 777, Terminal: B, Gate: B33, Direction: Berlin, Status: registration</p>
								</div>
								<img class="flight-logo" src="../image/departures-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">10:30</td>
							<td class="flight-big-cell">332 <span class="small-hide">Boeing 777</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">Alitalia</span><img class="airline-logo" src="../image/alitalia_big-1.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Berlin</td>
							<td class="flight-medium-cell">B Gate B33</td>
						</tr>
						<tr class="flight-row global-zebra-row departures departures-zebra-row">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/departures-flight.png" title="COMING" /> <p> 10:45 205, Plane: Airbus Industrie A380, Terminal: B, Gate: C7, Direction: Rome, Status: expected</p>
								</div>
								<img class="flight-logo" src="../image/departures-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">10:45</td>
							<td class="flight-big-cell">205 <span class="small-hide">Airbus A380</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">KLM</span><img class="airline-logo" src="../image/KLM_full.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Rome</td>
							<td class="flight-medium-cell">B Gate C7</td>
						</tr>
						<tr class="flight-row arrivals">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/arrivals-flight.png" title="COMING" /> <p> 11:00 016, Plane: Airbus Industrie A320, Terminal: A, Gate: C64, Direction: Samara, Status:  landed</p>
								</div>
								<img class="flight-logo" src="../image/arrivals-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">11:00</td>
							<td class="flight-big-cell">016 <span class="small-hide">Airbus A320</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">Aeroflot</span><img class="airline-logo" src="../image/aeroflot_full.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Samara</td>
							<td class="flight-medium-cell">A Gate C64</td>
						</tr>
						<tr class="flight-row global-zebra-row departures">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/departures-flight.png" title="COMING" /> <p> 11:10 156, Plane: Boeing 747, Terminal: B, Gate: A50, Direction: Rome, Status: all aboard</p>
								</div>
								<img class="flight-logo" src="../image/departures-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">11:10</td>
							<td class="flight-big-cell">156 <span class="small-hide">Boeing 747</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">Alitalia</span><img class="airline-logo" src="../image/alitalia_big-1.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Rome</td>
							<td class="flight-medium-cell">B Gate A50</td>
						</tr>
						<tr class="flight-row arrivals-zebra-row arrivals">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/arrivals-flight.png" title="COMING" /> <p> 11:25 734, Plane: Boeing 777, Terminal: C, Gate: B50, Direction: Helsinki, Status: expected</p>
								</div>
								<img class="flight-logo" src="../image/arrivals-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">11:25</td>
							<td class="flight-big-cell">734 <span class="small-hide">Boeing 777</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">KLM</span><img class="airline-logo" src="../image/KLM_full.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Helsinki</td>
							<td class="flight-medium-cell">C Gate B50</td>
						</tr>
						<tr class="flight-row global-zebra-row departures departures-zebra-row">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/departures-flight.png" title="COMING" /> <p> 11:40 324, Plane: Airbus Industrie A380, Terminal: A, Gate: A33, Direction: Berlin, Status: registration</p>
								</div>
								<img class="flight-logo" src="../image/departures-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">11:40</td>
							<td class="flight-big-cell">324 <span class="small-hide">Airbus A380</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">KLM</span><img class="airline-logo" src="../image/KLM_full.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Berlin</td>
							<td class="flight-medium-cell">A Gate A33</td>
						</tr>
						<tr class="flight-row departures">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/departures-flight.png" title="COMING" /> <p> 11:45 416, Plane: Airbus Industrie A320, Terminal: D, Gate: B12, Direction: Orenburg, Status: expected</p>
								</div>
								<img class="flight-logo" src="../image/departures-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">11:45</td>
							<td class="flight-big-cell">416 <span class="small-hide">Airbus A320</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">Aeroflot</span><img class="airline-logo" src="../image/aeroflot_full.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Orenburg</td>
							<td class="flight-medium-cell">D Gate B12</td>
						</tr>
						<tr class="flight-row global-zebra-row departures">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/departures-flight.png" title="COMING" /> <p> 11:50 332, Plane: Boeing 777, Terminal: B, Gate: B33, Direction: Berlin, Status: registration</p>
								</div>
								<img class="flight-logo" src="../image/departures-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">11:50</td>
							<td class="flight-big-cell">332 <span class="small-hide">Boeing 777</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">Alitalia</span><img class="airline-logo" src="../image/alitalia_big-1.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Berlin</td>
							<td class="flight-medium-cell">B Gate B33</td>
						</tr>
						<tr class="flight-row global-zebra-row departures departures-zebra-row">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/departures-flight.png" title="COMING" /> <p> 11:55 205, Plane: Airbus Industrie A380, Terminal: B, Gate: C7, Direction: Rome, Status: expected</p>
								</div>
								<img class="flight-logo" src="../image/departures-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">11:55</td>
							<td class="flight-big-cell">205 <span class="small-hide">Airbus A380</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">KLM</span><img class="airline-logo" src="../image/KLM_full.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Rome</td>
							<td class="flight-medium-cell">B Gate C7</td>
						</tr>
						<tr class="flight-row arrivals-zebra-row arrivals">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/arrivals-flight.png" title="COMING" /> <p> 11:25 734, Plane: Boeing 777, Terminal: C, Gate: B50, Direction: Helsinki, Status: expected</p>
								</div>
								<img class="flight-logo" src="../image/arrivals-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">11:25</td>
							<td class="flight-big-cell">734 <span class="small-hide">Boeing 777</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">KLM</span><img class="airline-logo" src="../image/KLM_full.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Helsinki</td>
							<td class="flight-medium-cell">C Gate B50</td>
						</tr>
						<tr class="flight-row global-zebra-row departures departures-zebra-row">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/departures-flight.png" title="COMING" /> <p> 11:40 324, Plane: Airbus Industrie A380, Terminal: A, Gate: A33, Direction: Berlin, Status: registration</p>
								</div>
								<img class="flight-logo" src="../image/departures-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">11:40</td>
							<td class="flight-big-cell">324 <span class="small-hide">Airbus A380</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">KLM</span><img class="airline-logo" src="../image/KLM_full.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Berlin</td>
							<td class="flight-medium-cell">A Gate A33</td>
						</tr>
						<tr class="flight-row departures">
							<td class="flight-small-cell auto-padding-cell">
								<div class="flight-row-popup">
									<img class="flight-popup-logo" src="../image/departures-flight.png" title="COMING" /> <p> 11:45 416, Plane: Airbus Industrie A320, Terminal: D, Gate: B12, Direction: Orenburg, Status: expected</p>
								</div>
								<img class="flight-logo" src="../image/departures-flight.png" title="COMING" />
							</td>
							<td class="flight-small-cell">11:45</td>
							<td class="flight-big-cell">416 <span class="small-hide">Airbus A320</span></td>
							<td class="medium-hide flight-big-cell"><span class="small-hide airline-name">Aeroflot</span><img class="airline-logo" src="../image/aeroflot_full.jpg" /></td>
							<td class="medium-hide flight-medium-cell">Orenburg</td>
							<td class="flight-medium-cell">D Gate B12</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="footer-stripe-buffer stripe-type-overhang"></div>
		</div>
		<div class="footer-container">
			<div class="footer-stripe stripe-type-overhang">
			</div>
		</div>
		@section('javascripts')
			<script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>
		@show
    </body>
</html>
