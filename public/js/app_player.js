var AudioPlayer = {

	frequencies : [60, 170, 310, 600, 1000, 3000, 6000, 12000, 14000, 16000],
	startOffset : 0,
	startTime : 0,
	context : null,
	files : null,
	filters: null,
	analyser: null,
	audioBufferSourceNode: null,
	soundBuffer: null,
	animationId: null,
	playing: false,
	tagList: null,
	init: function () {
		window.AudioContext = window.AudioContext || window.webkitAudioContext || window.mozAudioContext || window.msAudioContext;
        window.requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame;
        window.cancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelAnimationFrame || window.mozCancelAnimationFrame || window.msCancelAnimationFrame;
		try {
			AudioPlayer.context = new AudioContext();
		}
		catch(e) {
			alert('Web Audio API is not supported in this browser');
		}
		// Check for the various File API support.
		if (window.File && window.FileReader && window.FileList && window.Blob) {
		// Great success! All the File APIs are supported.
		} else {
			alert('The File APIs are not fully supported in this browser.');
		}
		
		
	},
	play: function (evt) {

		if(AudioPlayer.soundBuffer) {
			document.getElementById('play').classList.add('pause');
			document.getElementById('play').classList.remove('play');
			
			AudioPlayer.createBufferSourceNode();
			
			AudioPlayer.playing = true;
			AudioPlayer.startTime = AudioPlayer.context.currentTime;
			
			AudioPlayer.audioBufferSourceNode.start(0, AudioPlayer.startOffset % AudioPlayer.soundBuffer.duration);
			AudioPlayer.drawSpectrum();

			
			
			document.getElementById('play').removeEventListener('click', AudioPlayer.play, false);
			document.getElementById('play').addEventListener('click', AudioPlayer.pause, false);
		}
		
	},
	pause: function (evt) {


		document.getElementById('play').classList.add('play');
		document.getElementById('play').classList.remove('pause');
		
		AudioPlayer.playing = false;
		AudioPlayer.startOffset += AudioPlayer.context.currentTime - AudioPlayer.startTime;
		AudioPlayer.audioBufferSourceNode.stop(0);
		
		document.getElementById('play').removeEventListener('click', AudioPlayer.pause, false);
		document.getElementById('play').addEventListener('click', AudioPlayer.play, false);
		
		
	},
	stop: function (evt) {


		document.getElementById('play').classList.add('play');
		document.getElementById('play').classList.remove('pause');

		AudioPlayer.playing = false;
		AudioPlayer.audioBufferSourceNode.stop(0);
		AudioPlayer.startOffset = 0;
		 
		document.getElementById('play').removeEventListener('click', AudioPlayer.pause, false);
		document.getElementById('play').addEventListener('click', AudioPlayer.play, false);
		
		
	},
	load: function (evt) {
		output = '<strong>File loading ...</strong>';
		document.getElementById('list').innerHTML = output;
		if(AudioPlayer.files && AudioPlayer.files[0] != 'undefined') {
			var reader = new FileReader();	
			reader.onload = function(evt) {
				AudioPlayer.tagList = null;
				id3(AudioPlayer.files[0], function(err, tags) {
					AudioPlayer.tagList = tags;
				})
				
				if (AudioPlayer.soundBuffer && AudioPlayer.audioBufferSourceNode) {
					AudioPlayer.stop();	
				}
				
				var fileResult = evt.target.result;
				AudioPlayer.context.decodeAudioData(fileResult, function(soundBuffer){
					AudioPlayer.soundBuffer = soundBuffer;
					AudioPlayer.showPlaylist();
                    AudioPlayer.play();
				});

			};
			reader.readAsArrayBuffer(AudioPlayer.files[0]);
		}
	},
	createBufferSourceNode: function () {
		if (AudioPlayer.soundBuffer) {
			
			AudioPlayer.audioBufferSourceNode = AudioPlayer.context.createBufferSource();
		
			AudioPlayer.createEqualizer();
			
			//создаем и подключаем анализатор для визуализаций
			AudioPlayer.createAnalyser();
			
			//then assign the buffer to the buffer source node
			AudioPlayer.audioBufferSourceNode.buffer = AudioPlayer.soundBuffer;
			
			AudioPlayer.audioBufferSourceNode.onended = function() {
				var curOffset = AudioPlayer.startOffset;
				curOffset += AudioPlayer.context.currentTime - AudioPlayer.startTime;

				if(curOffset % AudioPlayer.soundBuffer.duration < 0.1) { //если трек полностью проигран
					AudioPlayer.stop();
				}
			}
		}  
		
	},
	createAnalyser: function () {
			
		AudioPlayer.analyser = AudioPlayer.context.createAnalyser();
	
		//connect the source to the analyser
		AudioPlayer.audioBufferSourceNode.connect( AudioPlayer.analyser);
		//connect the analyser to the destination(the speaker), or we won't hear the sound
		AudioPlayer.analyser.connect(AudioPlayer.context.destination);
		
	},
	showPlaylist: function () {
		
		var output = '';
		if(AudioPlayer.files && AudioPlayer.files[0] != 'undefined') {
			if(AudioPlayer.tagList && AudioPlayer.tagList.artist && AudioPlayer.tagList.title) {
				output = '<audio id="track"></audio><span class="track-title"><strong>' + AudioPlayer.tagList.title + '</strong></span>&nbsp&nbsp<span class="track-artist"><strong>by ' + AudioPlayer.tagList.artist + '</strong></span>';
			}else {
				output = '<audio id="track"></audio><strong>'+ AudioPlayer.files[0].name + '</strong>';
			}
			
		}		
		document.getElementById('list').innerHTML = output;
	},
	drawSpectrum: function () {
		
		if(AudioPlayer.analyser) {
			
			var canvas = document.getElementById('canvas'),
				cwidth = canvas.width,
				cheight = canvas.height - 2,
				meterWidth = 10, //width of the meters in the spectrum
				gap = 2, //gap between meters
				capHeight = 2,
				capStyle = '#fff',
				meterNum = 800 / (10 + 2), //count of the meters
				capYPositionArray = []; ////store the vertical position of hte caps for the preivous frame
				
			ctx = canvas.getContext('2d'),
			gradient = ctx.createLinearGradient(0, 0, 0, 300);
			gradient.addColorStop(1, '#0f0');
			gradient.addColorStop(0.5, '#ff0');
			gradient.addColorStop(0, '#f00');
			
			var drawMeter = function() {
				var array = new Uint8Array(AudioPlayer.analyser.frequencyBinCount);
				AudioPlayer.analyser.getByteFrequencyData(array);

 	             if (AudioPlayer.playing === false) {
					for (var i = array.length - 1; i >= 0; i--) {
						array[i] = 0;
					};
					allCapsReachBottom = true;
					for (var i = capYPositionArray.length - 1; i >= 0; i--) {
						allCapsReachBottom = allCapsReachBottom && (capYPositionArray[i] === 0);
					};
					if (allCapsReachBottom) {
						cancelAnimationFrame(AudioPlayer.animationId); //since the sound is top and animation finished, stop the requestAnimation to prevent potential memory leak,THIS IS VERY IMPORTANT!
						return;
					};
				};
				var step = Math.round(array.length / meterNum); //sample limited data from the total array
				ctx.clearRect(0, 0, cwidth, cheight);
				for (var i = 0; i < meterNum; i++) {
					var value = array[i * step];
					if (capYPositionArray.length < Math.round(meterNum)) {
						capYPositionArray.push(value);
					};
					ctx.fillStyle = capStyle;
					//draw the cap, with transition effect
					if (value < capYPositionArray[i] && capYPositionArray[i] != 1) {
						ctx.fillRect(i * 12, cheight - (--capYPositionArray[i]), meterWidth, capHeight);
					} else {
						ctx.fillStyle = '#2d3133';
						ctx.fillRect(i * 12, cheight - value, meterWidth, capHeight);
						capYPositionArray[i] = value;
					};
					
					ctx.fillStyle = gradient; //set the filllStyle to gradient for a better look
					ctx.fillRect(i * 12 /*meterWidth+gap*/ , cheight - value + capHeight, meterWidth, cheight); //the meter
				}
				AudioPlayer.animationId = requestAnimationFrame(drawMeter);
			}
			AudioPlayer.animationId = requestAnimationFrame(drawMeter);
		}
	},
	cleanDrawSpectrum: function () {
		var array = new Uint8Array(AudioPlayer.analyser.frequencyBinCount);
		AudioPlayer.analyser.getByteFrequencyData(array);
		 for (var i = array.length - 1; i >= 0; i--) {
			array[i] = 0;
		};
		allCapsReachBottom = true;
		for (var i = capYPositionArray.length - 1; i >= 0; i--) {
			allCapsReachBottom = allCapsReachBottom && (capYPositionArray[i] === 0);
		};
	},
	createFilter: function (frequency, gain) {
		var filter = AudioPlayer.context.createBiquadFilter();

		filter.type = 'peaking'; // тип фильтра
		filter.frequency.value = frequency; // частота
		filter.Q.value = 1; // Q-factor
		filter.gain.value = gain;
		return filter;
	},
	createEqualizer: function () {
		var radios = document.getElementsByName('equalizer');
		for (var i = 0; i < radios.length; i++) {
			if (radios[i].checked) {
				var equalizerType = radios[i].value;       
			}
		}

		if(equalizerType && AudioPlayer.equalizerTypes[equalizerType]) {
			
			var equalizer = AudioPlayer.equalizerTypes[equalizerType];
			var frequencies = AudioPlayer.frequencies;

			AudioPlayer.filters = [];
			frequencies.forEach(function(item, i, frequencies) {
				AudioPlayer.filters[i] = AudioPlayer.createFilter(item, equalizer[item]);
			});
			
			AudioPlayer.filters.reduce(function (prev, curr) {
				prev.connect(curr);
				return curr;
			});
			
			AudioPlayer.audioBufferSourceNode.connect(AudioPlayer.filters[0]);
			AudioPlayer.filters[AudioPlayer.filters.length - 1].connect(AudioPlayer.context.destination);
		}
		
	},
	changeEqualizer: function (equalizerType) {
		if(AudioPlayer.filters) {
			var radios = document.getElementsByName('equalizer');
			for (var i = 0; i < radios.length; i++) {
				if (radios[i].checked) {
					var equalizerType = radios[i].value;       
				}
			}
			if(equalizerType) {	
				if(equalizerType && AudioPlayer.equalizerTypes[equalizerType]) {
					var frequencies = AudioPlayer.frequencies;
					var equalizer = AudioPlayer.equalizerTypes[equalizerType];
					frequencies.forEach(function(item, i, frequencies) {
						AudioPlayer.filters[i].gain.value = equalizer[item];
					});		
				}
			}
		}
	},
	uploadFile: function (evt) {
		var files =  evt.target.files;
		if(files[0]) {
			AudioPlayer.files = files;
			AudioPlayer.load();
		}
	},
	handleFileSelect: function (evt) {
		evt.stopPropagation();
		evt.preventDefault();

		var files = evt.dataTransfer.files;
		if(files[0]) {
			AudioPlayer.files = files;
			AudioPlayer.load();
		}
	},
	handleDragOver: function (evt) {
		evt.stopPropagation();
		evt.preventDefault();
		evt.dataTransfer.dropEffect = 'copy'; 
	},
	equalizerTypes: {
		'pop': { '60':  '1' , '170':  '3' , '310':  '4' , '600':  '5' , '1000':  '4' , '3000':  '4' , '6000':  '1' , '12000':  '0' , '14000':  '0' , '16000':  '0'  },
		'classic': { '60':  '3' , '170':  '3' , '310':  '3' , '600':  '-3' , '1000':  '-3' , '3000':  '0' , '6000':  '4' , '12000':  '5' , '14000':  '0' , '16000':  '0'  },
		'normal': { '60': '0' , '170':  '0' , '310':  '0' , '600':  '0' , '1000':  '0' , '3000':  '0' , '6000':  '0' , '12000':  '0' , '14000':  '0' , '16000':  '0'  },
		'jazz': { '60':  '0' , '170':  '5' , '310':  '3' , '600':  '-3' , '1000':  '-3' , '3000':  '0' , '6000':  '2' , '12000':  '5' , '14000':  '0' , '16000':  '0'  },
		'rock': { '60':  '3' , '170':  '3' , '310':  '1' , '600':  '2' , '1000':  '3' , '3000':  '2' , '6000':  '4' , '12000':  '5' , '14000':  '0' , '16000':  '0'  }
	}
	
}

window.addEventListener('load', AudioPlayer.init, false);
document.getElementById('files').addEventListener('change', AudioPlayer.uploadFile, false);


document.getElementById('normal-equalizer').addEventListener('change', AudioPlayer.changeEqualizer, false);
document.getElementById('pop-equalizer').addEventListener('change', AudioPlayer.changeEqualizer, false);
document.getElementById('rock-equalizer').addEventListener('change', AudioPlayer.changeEqualizer, false);
document.getElementById('classic-equalizer').addEventListener('change',AudioPlayer.changeEqualizer, false);
document.getElementById('jazz-equalizer').addEventListener('change', AudioPlayer.changeEqualizer, false);



var dropZone = document.getElementById('player_container');
dropZone.addEventListener('dragover', AudioPlayer.handleDragOver, false);
dropZone.addEventListener('drop', AudioPlayer.handleFileSelect, false);

document.getElementById('play').addEventListener('click', AudioPlayer.play, false);

