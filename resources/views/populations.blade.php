<!DOCTYPE html>
<html lang="ru" id="nojs" xml:lang="ru">
    <head>
        <meta charset="utf-8" />
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="viewport" content="width=1024" />

        <title>@yield('title')</title>

        <link rel="shortcut icon" href="{!! asset('favicon.ico') !!}" type="image/x-icon" />
		@section('stylesheets')
            <!--[if IE 9 ]>
            <link rel="stylesheet" media="screen" href="{{ asset('css/sl-common.template_styles.ie9.css') }}" />
            <![endif]-->
            <link rel="stylesheet" media="screen" href="{!! asset('css/app.css') !!}" />
        @show
    </head>
	<body>
		<div class="page-container">
		  <div class="header-stripe stripe-type-overhang"></div>
		  <div class="content-stripe stripe-type-overhang"></div>
		<div class="footer-container">
			<div class="footer-stripe stripe-type-overhang"></div>
		</div>
		@section('javascripts')
			<script type="text/javascript" src="{!! asset('js/app_populations.js') !!}"></script>
		@show
    </body>
</html>
