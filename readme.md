## Demo site 

## Первое задание:

[CSS](https://gitlab.com/Arseny/demo/blob/master/public/css/app.css), [Template](https://gitlab.com/Arseny/demo/blob/master/resources/views/air.blade.php)

[Пример](http://jsfiddle.net/senya3/fpaat9zL)

## Второе задание:

[JS](https://gitlab.com/Arseny/demo/blob/master/public/js/app_populations.js)

[Пример](http://jsfiddle.net/senya3/1mxtj74r)

## Третье задание:

[CSS](https://gitlab.com/Arseny/demo/blob/master/public/css/app_player.css), [JS](https://gitlab.com/Arseny/demo/blob/master/public/js/app_player.js) , [Template](https://gitlab.com/Arseny/demo/blob/master/resources/views/player.blade.php)

[Пример](http://jsfiddle.net/senya3/yxrreae9)
